import seedrandom from "seedrandom";
import { Text } from "@mimer/text";

let random = seedrandom();

export interface question {
  description: Text;
  solution: number;
}

export function questionSeed(seed: string) {
  random = seedrandom(seed);
}

/**
 * The function will create a function that generat questions about
 * numbers. The questions is about adding two numbers.
 *
 * @param min Lowest values of the numbers.
 * @param max Higest values of the numbers.
 *
 * @return A function that create a question.
 */
export function questionAdd(min: number, max: number): () => question {
  return function (): question {
    let term_1 = Math.floor(random() * max) + min;
    let term_2 = Math.floor(random() * max) + min;

    return {
      description: new Text().add(String(term_1), "+", String(term_2)),
      solution: term_1 + term_2,
    };
  };
}

/**
 * The function will create a function that generat questions about
 * numbers. The questions is about multiplay two numbers.
 *
 * @param min Lowest values of the numbers.
 * @param max Higest values of the numbers.
 *
 * @return A function that create a question.
 */
export function questionMult(min: number, max: number): () => question {
  return function (): question {
    let term_1 = Math.floor(random() * max) + min;
    let term_2 = Math.floor(random() * max) + min;

    return {
      description: new Text().add(String(term_1), "*", String(term_2)),
      solution: term_1 * term_2,
    };
  };
}

/**
 * The function will create a function that generat questions about
 * numbers. The questions is about finding roots of numbers.
 *
 * @param min Lowest values of the numbers.
 * @param max Higest values of the numbers.
 *
 * @return A function that create a question.
 */
export function questionSqrt(min: number, max: number) {
  return function (): question {
    let term = Math.floor(random() * max) + min;

    return {
      description: new Text().add("Rotten av ", String(term * term)),
      solution: term,
    };
  };
}
