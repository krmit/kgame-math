#!/usr/bin/env node
import { server } from "@mimer/game-server";
import { KMath } from "./kgame-math";

const configServer = {
  port: 8080,
  simpel: true,
};

const configGame = {
  name: "kmathGame",
  length: 10,
  title: "Mate spelet k",
};

server(configServer, KMath, configGame);
