#!/usr/bin/env node
import { Text } from "@mimer/text";
import { questionAdd, questionMult, questionSqrt, question } from "./aritmetic";
import { Player, Game, GameConfig, GameStatus } from "@mimer/game-server";

export class KMath extends Game {
  questions: (() => question)[] = [];
  count = 0;
  points = 0;
  length: number;
  solution?: number;
  constructor(config: GameConfig) {
    super({ name: config.name ?? "Några räckne övningar" });
    this.length = config.length ?? 5;
    this.questions.push(questionAdd(2, 4));
    this.questions.push(questionAdd(4, 6));
    this.questions.push(questionAdd(4, 6));
    this.questions.push(questionMult(1, 4));
    this.questions.push(questionSqrt(1, 4));
  }

  start() {
    const msg = new Text();
    msg.append("Välkommen till vårt matespel!").headline;
    msg.add(this.doQuiestion());
    return msg;
  }

  next(player: Player, answer: { answer: string }): void | GameStatus {
    if (Number(answer.answer) === this.solution) {
      this.points++;
      player.msg.append(":) Rätt").good.line;
    } else {
      player.msg.append(":( Fel").bad.line;
    }

    if (this.questions.length === 0) {
      player.msg.append("Result: ").important;
      player.msg.append(String(this.points)).number;
      player.msg.append(" rätt av ");
      player.msg.append(String(this.count)).number;
      player.msg.append(" (");
      player.msg.append(String(Math.floor(this.points / this.count) * 100))
        .number;
      player.msg.append("% )");
      player.msg.line;
      return "end";
    }

    player.msg.add(this.doQuiestion());
  }

  doQuiestion() {
    const result = new Text();
    this.count++;
    const index = Math.floor(Math.random() * this.questions.length);
    const question = this.questions[index]();
    this.questions.splice(index, 1);
    console.log();
    this.solution = question.solution;
    result.append(question.description.add(": ")).question("answer");
    return result;
  }
}
