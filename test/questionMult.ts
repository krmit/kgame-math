import { assert } from "chai";
import { questionMult, questionSeed, question } from "../src/aritmetic";
/* Need refactoring */
/*
describe("Using questionMult", () => {
  let result: (string|number)[];



  describe('Test some smalle numbers', () => {
    beforeEach(function() {
      questionSeed("Test");
    });

    it("6*2", () => {
      const testQuestion = questionMult(2,5)();

      assert.deepEqual(
        testQuestion,  
        { description: [6, "*", 2],
                   solution: 12
                  }
      );
        });

    it("14*7", () => {
      const testQuestion = questionMult(6,9)();

      assert.deepEqual(
        testQuestion,  
        { description: [14, "*", 7],
                   solution: 98
                  }
      );
        });

        it("14*7", () => {
          const testQuestion = questionMult(6,9)();
    
          assert.deepEqual(
            testQuestion,  
            { description: [14, "*", 7],
                       solution: 98
                      }
          );
            });
        });

        describe('Test with special numbers', () => {
          it("0*1", () => {
            questionSeed("Test-1");
            const testQuestion = questionMult(0,3)();
      
            assert.deepEqual(
              testQuestion,  
              { description: [0, "*", 1],
                         solution: 0
                        }
            );
              });
      
          it("0+2", () => {
            questionSeed("Test-6");
            const testQuestion = questionMult(0,3)();
      
            assert.deepEqual(
              testQuestion,  
              { description: [0, "*", 2],
                         solution: 0
                        }
            );
              });
      
              it("2+1", () => {
                questionSeed("Test-9");
                const testQuestion = questionMult(0,3)();
          
                assert.deepEqual(
                  testQuestion,  
                  { description: [2, "*", 1],
                             solution: 2
                            }
                );
                  });
              });

              describe('Test big numbers', () => {
                it("Two numbers 10^5 and 10^6", () => {
                  questionSeed("Test");
                  const testQuestion = questionMult(100000,1000000)();
            
                  assert.deepEqual(
                    testQuestion,  
                    { description: [999081, "*", 257394                  ],
                               solution: 257157454914
                              }
                  );
                    });
            
                it("Two numbers 0 and 10^10", () => {
                  questionSeed("Test");
                  const testQuestion = questionMult(0,10**10)();
            
                  assert.deepEqual(
                    testQuestion,  
                    { description: [8990819543, "*", 1573940110],
                               solution: 14151011500499570000
                              }
                  );
                    });
            
                    it("Two numbers 0 and -10^10", () => {
                      questionSeed("Test-1");
                      const testQuestion = questionMult(0,-(10**10))();
                
                      assert.deepEqual(
                        testQuestion,  
                        { description: [-1650019080, "*", -6431160217],
                                   solution: 10611537064586940000
                                  }
                      );
                        });
                    });
    });
*/
