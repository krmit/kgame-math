import { assert } from "chai";
import { Text } from "@mimer/text";
import { questionAdd, questionSeed, question } from "../src/aritmetic";

/* Need refactoring */
/*
describe("Using questionAdd", () => {
  let result: (string|number)[];



  describe('Test some smalle numbers', () => {
    beforeEach(function() {
      questionSeed("Test");
    });

    it("6+2", () => {
      const testQuestion = questionAdd(2,5)();

      assert.deepEqual(
        testQuestion,  
        { description: new Text().add(String(6), "+", String(2)),
                   solution: 8
                  }
      );
        });

    it("14+7", () => {
      const testQuestion = questionAdd(6,9)();

      assert.deepEqual(
        testQuestion,  
        { description: [14, "+", 7],
                   solution: 21
                  }
      );
        });

        it("14+7", () => {
          const testQuestion = questionAdd(6,9)();
    
          assert.deepEqual(
            testQuestion,  
            { description: [14, "+", 7],
                       solution: 21
                      }
          );
            });
        });

        describe('Test with special numbers', () => {
          it("0+1", () => {
            questionSeed("Test-1");
            const testQuestion = questionAdd(0,3)();
      
            assert.deepEqual(
              testQuestion,  
              { description: [0, "+", 1],
                         solution: 1
                        }
            );
              });
      
          it("0+2", () => {
            questionSeed("Test-6");
            const testQuestion = questionAdd(0,3)();
      
            assert.deepEqual(
              testQuestion,  
              { description: [0, "+", 2],
                         solution: 2
                        }
            );
              });
      
              it("2+1", () => {
                questionSeed("Test-9");
                const testQuestion = questionAdd(0,3)();
          
                assert.deepEqual(
                  testQuestion,  
                  { description: [2, "+", 1],
                             solution: 3
                            }
                );
                  });
              });

              describe('Test big numbers', () => {
                it("Two numbers 10^5 and 10^6", () => {
                  questionSeed("Test");
                  const testQuestion = questionAdd(100000,1000000)();
            
                  assert.deepEqual(
                    testQuestion,  
                    { description: [999081, "+", 257394                  ],
                               solution: 1256475
                              }
                  );
                    });
            
                it("Two numbers 0 and 10^10", () => {
                  questionSeed("Test");
                  const testQuestion = questionAdd(0,10**10)();
            
                  assert.deepEqual(
                    testQuestion,  
                    { description: [8990819543, "+", 1573940110],
                               solution: 10564759653
                              }
                  );
                    });
            
                    it("Two numbers 0 and -10^10", () => {
                      questionSeed("Test-1");
                      const testQuestion = questionAdd(0,-(10**10))();
                
                      assert.deepEqual(
                        testQuestion,  
                        { description: [-1650019080, "+", -6431160217],
                                   solution: -8081179297
                                  }
                      );
                        });
                    });
    });
*/
